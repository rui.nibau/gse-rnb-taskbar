---
title:      gse : rnb-taskbar
banner:     /lab/gse-rnb-taskbar/pics/1-0-0.jpg
date:       2022-03-31
updated:    2023-04-22
cats:       [informatique]
tags:       [gnome, gnome-shell, extensions, javascript]
techs:      [javascript, gnome]
version:    1.3.0
source:     https://framagit.org/rui.nibau/gse-rnb-taskbar
issues:     https://framagit.org/rui.nibau/gse-rnb-taskbar/-/issues
download:   /lab/gse-rnb-taskbar/build/latest/rnb-taskbar.zip
intro:      A simple taskbar for gnome-shell
itemtype:   SoftwareApplication
---

## Install from zip

• [Download the zip file](/lab/gse-rnb-taskbar/build/latest/rnb-taskbar.zip) and unzip it.
• copy the folder ``rnb-taskbar@omacronides.com`` into ``~/.local/share/gnome-shell/extensions/``.

## Install from source

°°stx-bash°°
    # Download sources
    git clone https://framagit.org/rui.nibau/gse-rnb-taskbar.git
    # Go to directory
    cd gse-rnb-taskbar
    # Checkout version to install
    git checkout tags/<version>
    # Install the extension using npm
    npm run deploy

## Features

• Removes the acivities menu.
• Display button for applications overview.
• Display favorite applications.
• Display running applications.
• Indicates the number of opened windows for each application.
• click on icon : open window | minimize/restore window | cycle through windows.
• righ-click on icon : Application menu.
• Opens maximized windows in the last (empty) workspace on the primary monitor.

## What will maybe be in it

[*] Dots for the number of opened windows.
[ ] Preferences/configuration

## What will never (?) be in it

• Window preview because I never use them.
• Reordering or drag&drop because it can be done with the dash.
• Workspace isolation because I don't use it often.

Il you want those features and much more, you can fork the project (it's open source) or you can use **[Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/)** or **[Dash to Panel](https://extensions.gnome.org/extension/1160/dash-to-panel/)**.

## Hack

°°todo°°A   écrire...

## Ressources et references

• [Développement javascript sous Gnome](/articles/gjs)

## History

..include::./changelog.md

## Licence

..include::./licence.md

