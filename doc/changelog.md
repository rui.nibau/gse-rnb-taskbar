---
title:      Historique
date:       2022-03-31
updated:    2023-04-22
---

°°changelog°°
1.3.0 øø(2023-04-22)øø:
    • upd: Gnome 44
1.2.0 øø(2022-05-26)øø:
    • add: Maximized window in the last (empty) workspace
1.1.1 øø(2022-04-24)øø:
    • fix: ([#1](https://framagit.org/rui.nibau/gse-rnb-taskbar/-/issues/1)) Wrong number of windows.
1.1.0 øø(2022-04-23)øø:
    • add: Indicators.
1.0.0 øø(2022-04-10)øø:
    • add: First publication.
