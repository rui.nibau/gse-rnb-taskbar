#!/bin/bash

# vars
VER=$npm_package_version
NAME=$npm_package_name
FULL_NAME=$NAME"@omacronides.com"

# Répertoires
DIR="$( cd "$( dirname "$0" )" && pwd )"
BUILD_DIR=$DIR"/build/"$VER
LATEST_DIR=$DIR"/build/latest"
SRC_DIR=$DIR"/src/"$FULL_NAME

build() {
    echo -e "Building "$NAME" "$VER"..."
    # clean
    if [[ -d $BUILD_DIR ]]; then
        rm -fr $BUILD_DIR
    fi
    if [[ -d $LATEST_DIR ]]; then
        rm -fr $LATEST_DIR
    fi
    # build
    mkdir $BUILD_DIR
    cd $BUILD_DIR
    cp -pr -t $BUILD_DIR $SRC_DIR
    zip -r $NAME".zip" $FULL_NAME
    cp -pr $BUILD_DIR $LATEST_DIR
    echo -e "Building done."
}

install() {
    echo -e "Installing "$NAME" "$VER"..."
    build
    rm -rf ~/.local/share/gnome-shell/extensions/$FULL_NAME
    cd $BUILD_DIR
    cp -r $FULL_NAME ~/.local/share/gnome-shell/extensions/.
    echo -e "Install done."
}

symlink() {
    echo -e "Symlink "$NAME" "$VER"..."
    cd $DIR
    rm -rf ~/.local/share/gnome-shell/extensions/$FULL_NAME
    ln -s src/$FULL_NAME ~/.local/share/gnome-shell/extensions/$FULL_NAME
}

# args
case $1 in
    build) build;;
    install) install;;
    symlink) symlink;;
esac
