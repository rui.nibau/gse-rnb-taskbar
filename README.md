# rnb-taskbar

* [website](https://omacronides.com/projets/gse-rnb-taskbar)
* [Sources](https://framagit.org/rui.nibau/gse-rnb-taskbar)
* [Issues](https://framagit.org/rui.nibau/gse-rnb-taskbar/-/issues)

Project is canceled, replaced by [rnb-panel](https://omacronides.com/projets/gse-rnb-panel/)
